<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\City;
use App\Gallery;
use App\Mail\ContactUs;
use App\Mail\PreSaleQuery;
use App\Package;
use App\Profile;
use App\Review;
use App\Timing;
use App\Category;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use mysql_xdevapi\Session;

class PublicView extends Controller
{
    public function PublicProfile(Request $request, $id, $name)
    {
        $user = User::find($id);
        $profile = $user->profile;

        if($request->ajax()){
            return response()->json($profile);
        }

        $gallery = $profile->gallery;
        $timings = $profile->timing;
        $p1 = $profile->package;
        $packages = $p1->reject(function ($p1) {
            return $p1->display == 0;
        });
        $cprs = $profile->Cpr;
        $profile_cat = preg_split ("/\,/", $profile->category);
        $categories = Category::whereIn('id', $profile_cat)->get();
        $city = City::find($profile->city);
        $mail = $user->email;

        $all_reviews = Review::all();
        $reviews = collect();
        foreach ($all_reviews as $review)
        {
            if ($review->order->package->profile_user_id == $id)
                $reviews->add($review);
        }

        // dd($reviews);

        $r_count = count($reviews);
        $exp = 0;
        $friend = 0;
        $overall = 0;
        foreach ($reviews as $review)
        {
            $exp = $exp + $review->experience;
            $friend = $friend + $review->friendliness;
            $overall = (($exp +$friend) + $overall)/2;
        }
        $r_array = [];

        if ($r_count > 0)
        {
            $r_array = [
                'exp' => $exp/$r_count,
                'friend' => $friend/$r_count,
                'overall' => $overall/$r_count,
            ];
        }
        else
        {
            $r_array = [
                'exp' => 0,
                'friend' => 0,
                'overall' => 0,
            ];
        }


        if(Auth::id())
        {
            $bookmark = count(Bookmark::where('profile_user_id', Auth::id())->where('bookmark_id', $profile->user_id)->get());
        }

        $keywords = preg_split ("/\,/", $profile->keywords);

        return view('trainer.public' , compact('profile' , 'gallery' , 'timings', 'categories', 'city', 'mail', 'keywords', 'packages', 'bookmark', 'r_array', 'reviews', 'cprs'));

    }

    public function FormSearch(Request $request)
    {
        return redirect()->route('TrainerResult', [$request->gender, json_encode($request->location), json_encode($request->specialty), json_encode($request->days), json_encode($request->time), $request->zip]);
    }


    public function TrainerResult($gender, $location, $specialty, $days, $timings, $zip)
    {
        $location_preference = json_decode($location, true);
        $specialty_preference = json_decode($specialty, true);
        $days_preference = json_decode($days, true);
        $time_preference = json_decode($timings, true);
        $profiles = Profile::with('User', 'Gallery', 'City')->get();
        $gender_filtered = $profiles;

        if($gender && $gender != "DoesntMatter")
        {
            $gender_filtered = null;
            $gender_filtered = collect();
            foreach ($profiles as $profile) {
                if ($profile->gender == $gender) {
                    $gender_filtered->add($profile);
                }
            }
        }


        $zip_filter = $gender_filtered;
        if($zip)
        {
            $zip_filter = null;
            $zip_filter = collect();
            foreach ($gender_filtered as $profile)
            {
                if($profile->zip == $zip)
                {
                    $zip_filter->add($profile);
                }
            }
        }


        $location_filter = $zip_filter;
        if(count($location_preference) > 0)
        {
            $location_filter = null;
            $location_filter = collect();

            foreach ($zip_filter as $profile)
            {
                if(in_array($profile->location_preference, $location_preference))
                {
                    $location_filter->add($profile);
                }
            }
        }


        $days_filter = $location_filter;
        if(count($days_preference) > 0 && $time_preference > 0)
        {
            $days_filter = null;
            $days_filter = collect();

            foreach ($location_filter as $profile)
            {
                $match = false;

                foreach ($days_preference as $day_preference){
                    switch ($day_preference) {
                        case "Monday":
                            if ($profile->Timing->monday_opening){
                                foreach ($time_preference as $time){
                                    switch ($time) {
                                        case "Morning":
                                            if ($profile->Timing->monday_opening <= 5 && $profile->Timing->monday_closing >= 11) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Afternoon":
                                            if ($profile->Timing->monday_opening <= 11 && $profile->Timing->monday_closing >= 16) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Evening":
                                            if ($profile->Timing->monday_opening <= 16 && $profile->Timing->monday_closing >= 21) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        default:
                                            if ($profile->Timing->monday_opening <= 21 && $profile->Timing->monday_closing >= 5) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                    }

                                    if ($match){
                                        break;
                                    }
                                }
                            } else{
                                $match = false;
                            }
                            break;
                        case "Tuesday":
                            if ($profile->Timing->tuesday_opening){
                                foreach ($time_preference as $time){
                                    switch ($time) {
                                        case "Morning":
                                            if ($profile->Timing->tuesday_opening <= 5 && $profile->Timing->tuesday_closing >= 11) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Afternoon":
                                            if ($profile->Timing->tuesday_opening <= 11 && $profile->Timing->tuesday_closing >= 16) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Evening":
                                            if ($profile->Timing->tuesday_opening <= 16 && $profile->Timing->tuesday_closing >= 21) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        default:
                                            if ($profile->Timing->tuesday_opening <= 21 && $profile->Timing->tuesday_closing >= 5) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                    }

                                    if ($match){
                                        break;
                                    }
                                }
                            } else{
                                $match = false;
                            }
                            break;
                        case "Wednesday":
                            if ($profile->Timing->wednesday_opening){
                                foreach ($time_preference as $time){
                                    switch ($time) {
                                        case "Morning":
                                            if ($profile->Timing->wednesday_opening <= 5 && $profile->Timing->wednesday_closing >= 11) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Afternoon":
                                            if ($profile->Timing->wednesday_opening <= 11 && $profile->Timing->wednesday_closing >= 16) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Evening":
                                            if ($profile->Timing->wednesday_opening <= 16 && $profile->Timing->wednesday_closing >= 21) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        default:
                                            if ($profile->Timing->wednesday_opening <= 21 && $profile->Timing->wednesday_closing >= 5) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                    }

                                    if ($match){
                                        break;
                                    }
                                }
                            } else{
                                $match = false;
                            }
                            break;
                        case "Thursday":
                            if ($profile->Timing->thursday_opening){
                                foreach ($time_preference as $time){
                                    switch ($time) {
                                        case "Morning":
                                            if ($profile->Timing->thursday_opening <= 5 && $profile->Timing->thursday_closing >= 11) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Afternoon":
                                            if ($profile->Timing->thursday_opening <= 11 && $profile->Timing->thursday_closing >= 16) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Evening":
                                            if ($profile->Timing->thursday_opening <= 16 && $profile->Timing->thursday_closing >= 21) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        default:
                                            if ($profile->Timing->thursday_opening <= 21 && $profile->Timing->thursday_closing >= 5) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                    }

                                    if ($match){
                                        break;
                                    }
                                }
                            } else{
                                $match = false;
                            }
                            break;
                        case "Friday":
                            if ($profile->Timing->friday_opening){
                                foreach ($time_preference as $time){
                                    switch ($time) {
                                        case "Morning":
                                            if ($profile->Timing->friday_opening <= 5 && $profile->Timing->friday_closing >= 11) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Afternoon":
                                            if ($profile->Timing->friday_opening <= 11 && $profile->Timing->friday_closing >= 16) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Evening":
                                            if ($profile->Timing->friday_opening <= 16 && $profile->Timing->friday_closing >= 21) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        default:
                                            if ($profile->Timing->friday_opening <= 21 && $profile->Timing->friday_closing >= 5) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                    }

                                    if ($match){
                                        break;
                                    }
                                }
                            } else{
                                $match = false;
                            }
                            break;
                        case "Saturday":
                            if ($profile->Timing->saturday_opening){
                                foreach ($time_preference as $time){
                                    switch ($time) {
                                        case "Morning":
                                            if ($profile->Timing->saturday_opening <= 5 && $profile->Timing->saturday_opening >= 11) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Afternoon":
                                            if ($profile->Timing->saturday_opening <= 11 && $profile->Timing->saturday_opening >= 16) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Evening":
                                            if ($profile->Timing->saturday_opening <= 16 && $profile->Timing->saturday_opening >= 21) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        default:
                                            if ($profile->Timing->saturday_opening <= 21 && $profile->Timing->saturday_opening >= 5) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                    }

                                    if ($match){
                                        break;
                                    }
                                }
                            } else{
                                $match = false;
                            }
                            break;
                        default:
                            if ($profile->Timing->sunday_opening){
                                foreach ($time_preference as $time){
                                    switch ($time) {
                                        case "Morning":
                                            if ($profile->Timing->sunday_opening <= 5 && $profile->Timing->sunday_closing >= 11) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Afternoon":
                                            if ($profile->Timing->sunday_opening <= 11 && $profile->Timing->sunday_closing >= 16) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        case "Evening":
                                            if ($profile->Timing->sunday_opening <= 16 && $profile->Timing->sunday_closing >= 21) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                            break;
                                        default:
                                            if ($profile->Timing->sunday_opening <= 21 && $profile->Timing->sunday_closing >= 5) {
                                                $match = true;
                                            } else {
                                                $match = false;
                                            }
                                    }

                                    if ($match){
                                        break;
                                    }
                                }
                            } else{
                                $match = false;
                            }
                    }

                    if (!$match){
                        break;
                    }
                }

                if ($match){
                    $days_filter->add($profile);
                }
            }
        }

        $specialty_filter = $days_filter;
        if(count($specialty_preference)>0)
        {
            $specialty_filter = null;
            $specialty_filter = collect();

            foreach ($days_filter as $profile)
            {
                $profile_cat = preg_split ("/\,/", $profile->category);
                $categories = Category::whereIn('id', $profile_cat)->get();

                foreach ($categories as $category)
                {
                    if(in_array($category->category_name, $specialty_preference))
                    {
                        $specialty_filter->add($profile);
                        break;
                    }
                }

            }
        }

        $specialty_filter = json_encode($specialty_filter);

        // Get all categories
        $categories = Category::all();

        return view('trainer.search', compact('specialty_filter', 'categories'));
    }


    public function CreateOrder($id)
    {
        $package = Package::find($id);

        if(Auth::id() == $package->profile_user_id)
        {
            \Session::flash('FailedPurchase', 'You are not allowed to perform this action');
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }

        $profile = Profile::find($package->profile_user_id);
        return view('order.checkout', compact('package', 'profile'));
    }

    public function FinalizeOrder(Request $request)
    {
        // Validate form data
        $this->validate($request, [
            'paypal_details' => 'required',
            'fname' => 'required|string',
            'lname' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'package_id' => 'required|integer',
        ]);

        // Json decode paypal_details
        $paypal_details = json_decode($request->paypal_details);

        $package = Package::find($request->package_id);
        $package->nulled = 1;
        $package->fresh = 1;
        $order = new Order;
        $order->first_name = $request->fname;
        $order->last_name = $request->lname;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->package_id = $request->package_id;
        $order->profile_user_id = Auth::id();
        $order->transaction_id = $paypal_details->id;
        $order->create_time = date('Y-m-d h:i:s', strtotime($paypal_details->create_time));
        $order->intent = $paypal_details->intent;
        $order->amount = $paypal_details->purchase_units[0]->amount->value;
        $order->currency_code = $paypal_details->purchase_units[0]->amount->currency_code;
        $order->payee_email_address = $paypal_details->purchase_units[0]->payee->email_address;
        $order->payee_merchant_id = $paypal_details->purchase_units[0]->payee->merchant_id;
        $order->status = $paypal_details->status;
        $order->payer_country_code = $paypal_details->payer->address->country_code;
        $order->payer_email_address = $paypal_details->payer->email_address;
        $order->payer_name = $paypal_details->payer->name->given_name . ' ' . $paypal_details->payer->name->surname;
        $order->payer_id = $paypal_details->payer->payer_id;
        $order->save();
        $package->save();
        // dd($order);
        return redirect()->route('OrderConfirmation', ['id' => $order->id]);

    }


    public function OrderConfirmation($id)
    {
        $order = Order::find($id);
        return view('order.confirmation', compact('order'));
    }

    public function InvoiceDisplay($id)
    {
        $order = Order::find($id);
        $package = Package::find($order->package_id);
        $trainer = Profile::find($package->profile_user_id);
        return view('order.invoice', compact('order', 'trainer', 'package'));
    }

    public function Contact()
    {
        return view('contact');
    }

    public function FAQ()
    {
        return view('FAQ');
    }

    public function Partners()
    {
        return view('partners');
    }

    public function Privacy()
    {
        return view('privacy');
    }

    public function TOS()
    {
        return view('tos');
    }

    public function PreSale(Request $request)
    {

        // dd($request);

        $valueArray = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'query' => $request->question,
            'preferred' => $request->contact_radio,
        ];


        $ToProfile = User::find($request->profile_id);

        $query = new PreSaleQuery($valueArray);
        Mail::to($ToProfile->email)->send($query);
        \Session::flash('message', 'Your query has been forwarded to the Trainer, you will hear back soon. Thanks!');
        \Session::flash('alert-class', 'alert-success');
        return redirect()->route('PublicProfile', ['id' => $ToProfile->id, 'name' => $ToProfile->name]);
    }

    public function ContactUs(Request $request)
    {

        $valueArray = [
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'query' => $request->comments,
        ];

        $query = new ContactUs($valueArray);
        Mail::to("info@pumpinghearts.net")->send($query);
        \Session::flash('message', 'Your query has been forwarded to the staff, you will hear back from us very soon. Thanks!');
        \Session::flash('alert-class', 'alert-success');
        return redirect()->route('contact');
    }

    public function filterTrainer(Request $request)
    {
        $profiles = Profile::with('User', 'Gallery', 'City')->get();

        $specialty_filter = $profiles;
        if (!isset($request->category_all)){
            if(isset($request->category))
            {
                $specialty_filter = null;
                $specialty_filter = collect();

                foreach ($profiles as $profile)
                {
                    $profile_cat = preg_split ("/\,/", $profile->category);
                    $categories = Category::whereIn('id', $profile_cat)->get();

                    foreach ($categories as $category)
                    {
                        if(in_array($category->category_name, $request->category))
                        {
                            $specialty_filter->add($profile);
                            break;
                        }
                    }

                }
            }
        }

        $keywords_filter = $specialty_filter;
        if(isset($request->keywords))
        {
            $keywords_filter = null;
            $keywords_filter = collect();

            foreach ($specialty_filter as $profile)
            {
                if (strpos($profile->keywords, $request->keywords) !== false) {
                    $keywords_filter->add($profile);
                }
            }
        }

        $location_filter = $keywords_filter;
        if(isset($request->lat) && $request->lat != null)
        {
            $location_filter = null;
            $location_filter = collect();

            foreach ($keywords_filter as $profile)
            {
                if ($profile->lat == $request->lat && $profile->long == $request->long) {
                    $location_filter->add($profile);
                }
            }
        }

        return response()->json($location_filter);
    }
}


