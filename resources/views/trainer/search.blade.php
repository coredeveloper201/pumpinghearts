@extends('layouts.main')

@section('head')

    <title>Pumping Hearts | Trainers</title>

@stop


@section('body')
    <script type="text/javascript">
        document.getElementById("body").classList.remove('transparent-header');
        document.getElementById("main-logo").src="{{asset('images/Logo-red-200px.png')}}";
        document.getElementById("header-container").classList.add('fixed');
        document.getElementById("header-container").classList.add('fullwidth');
        document.getElementById("header").classList.add('not-sticky');
    </script>

    <!-- Content
    ================================================== -->
    <div class="fs-container" id="app">

        <div id="work" class="fs-inner-container content">
            <div class="fs-content" >
                <!-- Search -->
                <section class="search">

                    <div class="row">
                        <div class="col-md-12">

                            <form id="filter-form" action="{{ route('filter.trainer') }}">

                                <!-- Row With Forms -->
                                <div class="row with-forms">

                                    <!-- Main Search Input -->
                                    <div class="col-fs-6">
                                        <div class="input-with-icon">
                                            <i class="sl sl-icon-magnifier"></i>
                                            <input type="text" placeholder="What are you looking for?" @keyup="filter" @change="filter" name="keywords" value=""/>
                                        </div>
                                    </div>

                                    <!-- Main Search Input -->
                                    <div class="col-fs-6">
                                        <div class="input-with-icon location">
                                            <div id="autocomplete-container">
                                                <input id="autocomplete-input" type="text" placeholder="Location">
                                            </div>
                                            <a href="#"><i class="fa fa-map-marker"></i></a>
                                        </div>
                                    </div>


                                    <!-- Filters -->
                                    <div class="col-fs-12">

                                        <!-- Panel Dropdown / End -->
                                        <div class="panel-dropdown">
                                            <a href="#">Categories</a>
                                            <div class="panel-dropdown-content checkboxes categories">

                                                <!-- Checkboxes -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="check-0" type="checkbox" name="category_all" @change="filter" class="all">
                                                        <label for="check-0">All Categories</label>
                                                    </div>
                                                    @foreach($categories as $category)
                                                    <div class="col-md-6">
                                                        <input id="check-{{ $category->id }}" type="checkbox" @change="filter" name="category[]" value="{{ $category->category_name }}">
                                                        <label for="check-{{ $category->id }}">{{ $category->category_name }}</label>
                                                    </div>
                                                    @endforeach
                                                </div>

                                                <!-- Buttons -->
                                                <div class="panel-buttons">
                                                    <button class="panel-cancel">Cancel</button>
                                                    <button class="panel-apply">Apply</button>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- Panel Dropdown / End -->

                                        <!-- Panel Dropdown -->
                                        <div class="panel-dropdown wide">
                                            <a href="#">More Filters</a>
                                            <div class="panel-dropdown-content checkboxes">

                                                <!-- Checkboxes -->
                                                <div class="row">
                                                    <div class="col-md-6">

                                                    </div>

                                                    <div class="col-md-6">
                                                        <input id="check-e" type="checkbox" name="check" >
                                                        <label for="check-e">Free parking on premises</label>

    {{--                                                    <input id="check-f" type="checkbox" name="check" >--}}
    {{--                                                    <label for="check-f">Free parking on street</label>--}}

    {{--                                                    <input id="check-g" type="checkbox" name="check">--}}
    {{--                                                    <label for="check-g">Smoking allowed</label>--}}

    {{--                                                    <input id="check-h" type="checkbox" name="check">--}}
    {{--                                                    <label for="check-h">Events</label>--}}
                                                    </div>
                                                </div>

                                                <!-- Buttons -->
                                                <div class="panel-buttons">
                                                    <button class="panel-cancel">Cancel</button>
                                                    <button class="panel-apply">Apply</button>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- Panel Dropdown / End -->

                                        <!-- Panel Dropdown -->
                                        <div class="panel-dropdown">
                                            <a href="#">Distance Radius</a>
                                            <div class="panel-dropdown-content">
                                                <input class="distance-radius" type="range" min="1" max="100" step="1" value="50" data-title="Radius around selected destination">
                                                <div class="panel-buttons">
                                                    <button class="panel-cancel">Disable</button>
                                                    <button class="panel-apply">Apply</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Panel Dropdown / End -->

                                    </div>
                                    <!-- Filters / End -->

                                </div>
                                <!-- Row With Forms / End -->
                            </form>

                        </div>
                    </div>

                </section>
                <!-- Search / End -->


                <section class="listings-container margin-top-30">
                    <!-- Sorting / Layout Switcher -->
                    <div class="row fs-switcher">

                        <div class="col-md-6">
                            <!-- Showing Results -->
                            <p class="showing-results" v-if="users.length > 0">@{{ users.length }} Results Found </p>
                            <p class="showing-results" v-else="">0 Results Found </p>
                        </div>

                    </div>


                    <!-- Listings -->
                    <div class="row fs-listings">
                        <!-- Listing Item -->
                        <div class="col-lg-12 col-md-12" v-for="user in users">
                            <div class="listing-item-container list-layout" data-marker-id="2">
                                <a :href="base_url + '/profile/' + user.user.id + '/' + user.user.name" class="listing-item">
                                    <!-- Image -->
                                    <div class="listing-item-image">
                                        <img :src="base_url + '/images/ProfileGallery/' + user.gallery[0].name" alt="" v-if="user.gallery.length > 0">
                                        <img src="{{asset('images/listing-item-01.jpg')}}" alt="" v-else="">
                                        <span class="tag">@{{user.city.name}}</span>
                                    </div>
                                    <!-- Content -->
                                    <div class="listing-item-content">
                                        <div class="listing-item-inner">
                                            <h3>@{{user.title}}</h3>
                                            <span>@{{user.address}}</span>
                                            <div class="star-rating" data-rating="5.0">
                                                <div class="rating-counter">(117 reviews)</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- Listing Item / End -->
                    </div>
                    <!-- Listings Container / End -->


{{--                    <!-- Pagination Container -->--}}
{{--                    <div class="row fs-listings">--}}
{{--                        <div class="col-md-12">--}}

{{--                            <!-- Pagination -->--}}
{{--                            <div class="clearfix"></div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-12">--}}
{{--                                    <!-- Pagination -->--}}
{{--                                    <div class="pagination-container margin-top-15 margin-bottom-40">--}}
{{--                                        <nav class="pagination">--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="#" class="current-page">1</a></li>--}}
{{--                                                <li><a href="#">2</a></li>--}}
{{--                                                <li><a href="#">3</a></li>--}}
{{--                                                <li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>--}}
{{--                                            </ul>--}}
{{--                                        </nav>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="clearfix"></div>--}}
{{--                            <!-- Pagination / End -->--}}

{{--                            <!-- Copyrights -->--}}
{{--                            <div class="copyrights margin-top-0">© 2019 Pumping Hearts. All Rights Reserved.</div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
                    <!-- Pagination Container / End -->
                </section>

            </div>
        </div>
        <div class="fs-inner-container map-fixed">

            <!-- Map -->
            <div id="map-container">
                <div id="map_view" style="width: 100%;height: 100%;"></div>
                {{--<div id="map" data-map-zoom="9" data-map-scroll="true">
                    <!-- map goes here -->
                </div>--}}
            </div>

        </div>
    </div>


@stop


@section('script')


    <!-- Maps -->
    {{--<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"></script>--}}
    <script type="text/javascript" src="{{asset('scripts/infobox.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('scripts/markerclusterer.js')}}"></script>
    <script type="text/javascript" src="{{asset('scripts/maps.js')}}"></script>

    <script type="text/javascript">
        var x = document.getElementById("footer");
        x.remove(x.selectedIndex);

        new Vue({
            el: '#app',
            data: {
                auth: window.auth,
                base_url : window.location.origin,
                location: {
                    formatted_address: null,
                    lat: null,
                    long: null,
                },
                map: null,
                users: '<?php echo $specialty_filter; ?>',
            },
            methods: {
                generateAutoComplete: function () {
                    let _this = this;
                    this.autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete-input'), {types: ['geocode']});
                    this.autocomplete.addListener('place_changed', function () {
                        let place = _this.autocomplete.getPlace();
                        _this.location.formatted_address = place.formatted_address;
                        _this.location.lat = place.geometry.location.lat();
                        _this.location.long = place.geometry.location.lng();
                        _this.filter();
                    })
                },
                filter: function () {
                    let _this = this;
                    var form = $("#filter-form");
                    var formData = $(form).serializeArray();
                    formData.push({ name: "lat", value: _this.location.lat }, { name: "long", value: _this.location.long });

                    // Submit the form using AJAX.
                    $.ajax({
                        type: 'GET',
                        url: $(form).attr('action'),
                        data: formData,
                        success: function(data){
                            _this.users = data;
                            _this.getUserData();
                        }
                    });
                },
                getUserData: function () {
                    let _this = this;

                    _this.map = new google.maps.Map(document.getElementById('map_view'), {
                        center: {lat: 23.7439732, lng: 90.3705565},
                        zoom: 14
                    });

                    for (let index = 0 ; index < _this.users.length; ++index) {
                        let lat = parseFloat(_this.users[index].lat);
                        let long = parseFloat(_this.users[index].long);
                        let name = _this.users[index].title;
                        let image = _this.users[index].picture;

                        var infowindow = new google.maps.InfoWindow({
                            content: name
                        });
                        var marker = new google.maps.Marker({
                            position: {lat: lat, lng: long},
                            map: _this.map,
                            icon: image,
                            title: name
                        });
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.open(_this.map,marker);
                        });
                        google.maps.event.trigger(marker,'click');
                        var cityCircle = new google.maps.Circle({
                            strokeColor: '#1e1aff',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#fa88ff',
                            fillOpacity: 0.35,
                            map: _this.map,
                            center: {lat: lat, lng: long},
                            radius: 0.1 * 100
                        });
                    }
                }
            },
            created(){
                this.users = JSON.parse(this.users);
            },
            mounted() {
                this.getUserData();
                this.generateAutoComplete();
            }
        });
    </script>

@stop

{{--http://127.0.0.1:8000/search/gender/Male/location/%5B%22trainer%22%5D/specialties/%5B%22Bodybuilding%20Preparations%22,%22Fat%20Loss%22,%22Elderly%22%5D/days/%5B%22Wednesday%22%5D/timings/%5B%22Morning%22,%22Afternoon%22%5D/zip/54770--}}
