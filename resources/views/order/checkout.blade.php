@extends('layouts.main')

@section('head')

    <title>Pumping Hearts | Trainers</title>
    <style rel="stylesheet">
        .paypal-disabled {
            pointer-events: none;
            opacity: 0.5;
        }
    </style>
@stop


@section('body')
    <script type="text/javascript">
        document.getElementById("body").classList.remove('transparent-header');
        document.getElementById("main-logo").src = "{{asset('images/Logo-red-200px.png')}}";
        // document.getElementById("header-container").classList.add('fixed');
        // document.getElementById("header-container").classList.add('fullwidth');
        // document.getElementById("header").classList.add('not-sticky');
    </script>


    <div class="clearfix"></div>
    <!-- Header Container / End -->


    <!-- Titlebar
    ================================================== -->
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Booking</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li>Booking</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>


    <!-- Content
    ================================================== -->

    <form id="checkOutForm" method="post" action="{{route('FinalizeOrder')}}">
    @csrf

    <!-- Container -->
        <div class="container">
            <div class="row">

                <!-- Content
                ================================================== -->
                <div class="col-lg-8 col-md-8 padding-right-30">

                    <h3 class="margin-top-0 margin-bottom-30">Personal Details</h3>
                    @if ($errors->has('paypal_details'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('paypal_details') }}
                        </div>
                    @endif
                    <input type="hidden" name="paypal_details" id="paypal_details">
                    <div class="row">

                        <div class="col-md-6">
                            <label>First Name</label>
                            <input onchange="checkValidation()" type="text" required name="fname" value="">
                        </div>

                        <div class="col-md-6">
                            <label>Last Name</label>
                            <input onchange="checkValidation()" type="text" required name="lname" value="">
                        </div>

                        <div class="col-md-6">
                            <div class="input-with-icon medium-icons">
                                <label>E-Mail Address</label>
                                <input onchange="checkValidation()" type="email" required name="email" value="">
                                <i class="im im-icon-Mail"></i>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-with-icon medium-icons">
                                <label>Phone</label>
                                <input onchange="checkValidation()" required type="text" name="phone" value="">
                                <i class="im im-icon-Phone-2"></i>
                            </div>
                        </div>

                    </div>


                    <h3 class="margin-top-55 margin-bottom-30">Payment Method</h3>

                    <!-- Payment Methods Accordion -->
                    <div class="payment" id="paypalLinks">

                        {{--<div class="payment-tab payment-tab-active">
                            <div class="payment-tab-trigger">
                                <input checked id="Test" name="cardType" type="radio" value="Test">
                                <label for="Test">Test</label>
    --}}{{--                            <img class="payment-logo paypal" src="https://i.imgur.com/ApBxkXU.png" alt="">--}}{{--
                            </div>

                            <div class="payment-tab-content">
                                <p>You will be redirected to complete payment.</p>
                            </div>
                        </div>--}}


                        {{--                    <div class="payment-tab payment-tab-active">--}}
                        {{--                        <div class="payment-tab-trigger">--}}
                        {{--                            <input checked id="paypal" name="cardType" type="radio" value="paypal">--}}
                        {{--                            <label for="paypal">PayPal</label>--}}
                        {{--                            <img class="payment-logo paypal" src="https://i.imgur.com/ApBxkXU.png" alt="">--}}
                        {{--                        </div>--}}

                        {{--                        <div class="payment-tab-content">--}}
                        {{--                            <p>You will be redirected to PayPal to complete payment.</p>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}


                        {{--                    <div class="payment-tab">--}}
                        {{--                        <div class="payment-tab-trigger">--}}
                        {{--                            <input type="radio" name="cardType" id="creditCart" value="creditCard">--}}
                        {{--                            <label for="creditCart">Credit / Debit Card</label>--}}
                        {{--                            <img class="payment-logo" src="https://i.imgur.com/IHEKLgm.png" alt="">--}}
                        {{--                        </div>--}}

                        {{--                        <div class="payment-tab-content">--}}
                        {{--                            <div class="row">--}}

                        {{--                                <div class="col-md-6">--}}
                        {{--                                    <div class="card-label">--}}
                        {{--                                        <label for="nameOnCard">Name on Card</label>--}}
                        {{--                                        <input id="nameOnCard" name="nameOnCard"  type="text">--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-md-6">--}}
                        {{--                                    <div class="card-label">--}}
                        {{--                                        <label for="cardNumber">Card Number</label>--}}
                        {{--                                        <input id="cardNumber" name="cardNumber" placeholder="1234  5678  9876  5432"  type="text">--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-md-4">--}}
                        {{--                                    <div class="card-label">--}}
                        {{--                                        <label for="expirynDate">Expiry Month</label>--}}
                        {{--                                        <input id="expiryDate" placeholder="MM"  type="text">--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-md-4">--}}
                        {{--                                    <div class="card-label">--}}
                        {{--                                        <label for="expiryDate">Expiry Year</label>--}}
                        {{--                                        <input id="expirynDate" placeholder="YY"  type="text">--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="col-md-4">--}}
                        {{--                                    <div class="card-label">--}}
                        {{--                                        <label for="cvv">CVV</label>--}}
                        {{--                                        <input id="cvv"  type="text">--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}

                    </div>
                    <!-- Payment Methods Accordion / End -->

                    <button type="submit" class="button booking-confirmation-btn margin-top-40 margin-bottom-65">Confirm
                        and Pay
                    </button>
                </div>


                <!-- Sidebar
                ================================================== -->
                <div class="col-lg-4 col-md-4 margin-top-0 margin-bottom-60">
                    <input style="display: none" type="text" name="package_id" value="{{$package->id}}">
                    <!-- Booking Summary -->
                    <div class="listing-item-container compact order-summary-widget" style="height: auto">
                        <div class="listing-item">
                            <?php

                            $images = \App\Gallery::where('profile_user_id', $profile->user_id)->first();
                            if ($images)
                                $image = 'images/ProfileGallery/' . $images->name;
                            else
                                $image = 'images/listing-item-01.jpg';
                            ?>
                            <img src="{{asset($image)}}" alt="">

                            <div class="listing-item-content">
                                <div class="numerical-rating" data-rating="5.0"></div>
                                <h3>{{$profile->title}}</h3>
                                <span>{{$profile->address}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="boxed-widget opening-hours summary margin-top-0">
                        <h3><i class="fa fa-calendar-check-o"></i> Booking Summary</h3>
                        <ul>
                            <li>Date <span>{{date("Y/m/d")}}</span></li>
                            <li>Package <span>{{$package->name}}</span></li>
                            <li>Description <span>{{$package->description}}</span></li>
                            <li class="total-costs">Total Cost <span>{{$package->price}}</span></li>
                        </ul>

                    </div>
                    <!-- Booking Summary / End -->

                </div>

            </div>
        </div>
        <!-- Container / End -->

    </form>

@stop


@section('script')


    <!-- Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"></script>
    <script type="text/javascript" src="{{asset('scripts/infobox.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('scripts/markerclusterer.js')}}"></script>
    <script type="text/javascript" src="{{asset('scripts/maps.js')}}"></script>

    {{--    <script type="text/javascript">--}}
    {{--        var x = document.getElementById("footer");--}}
    {{--        x.remove(x.selectedIndex);--}}
    {{--    </script>--}}

    <script>
        function checkValidation() {
            var form = $('#checkOutForm');
            var fname = form.find('input[name="fname"]').val();
            var lname = form.find('input[name="lname"]').val();
            var email = form.find('input[name="email"]').val();
            var phone = form.find('input[name="phone"]').val();
            console.log(phone);
            if ($.trim(fname).length > 0 && $.trim(phone).length > 0 &&
                $.trim(lname).length > 0 && $.trim(email).length > 0) {
                $('#paypalLinks').removeClass('paypal-disabled');
                return true;
            } else {
                $('#paypalLinks').addClass('paypal-disabled');
                return false;
            }
        }

        paypal.Buttons({
            createOrder: function (data, actions) {
                if (checkValidation()) {
                    return actions.order.create({
                        purchase_units: [
                            {
                                amount: {
                                    value: "{{$package->price}}"
                                }
                            }
                        ]
                    });
                }
            },
            onApprove: function (data, actions) {
                return actions.order.capture().then(function (details) {
                    console.log(details);
                    $('#paypal_details').val(JSON.stringify(details));
                    setTimeout(function () {
                        $('#checkOutForm')[0].submit();
                    }, 1000);
                });
            }
        }).render('#paypalLinks');
    </script>
@stop
